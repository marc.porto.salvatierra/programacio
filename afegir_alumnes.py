import sys
import ordena_alumnesDEF

d_tots = {'34567654A': {'nom': 'Mariona', 'cognom': 'fernandez', 'notes': [10, 2, 3, 4]},
 '42345678B': {'nom': 'pere', 'cognom': 'camps', 'notes': [1, 2, 3]}}


f = open(sys.argv[1],"r")

ordena_alumnesDEF.crea_diccionari_alumnes(f, d_tots)
f.close()
print('llistat amb alumnes nous:')
ordena_alumnesDEF.llistat_Alumnes_dic(d_tots)

taula = ordena_alumnesDEF.crea_taula(d_tots,'notes')


taula.sort(key = ordena_alumnesDEF.key_mitjana, reverse=True)
print(taula)

print('llistat ordenat per mitjana:')
ordena_alumnesDEF.llistat_Alumnes_taula(taula, d_tots)

f = open(sys.argv[1],"w")
ordena_alumnesDEF.desa_dades_alumnes(f,d_tots)
f.close()
