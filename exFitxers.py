import sys
# Fes un programa compti les línies d'un fitxer passat per argument.
if False:
	fitxer = open(sys.argv[1],"r")

	comptador = 0
	for linia in fitxer:
		comptador += 1
	
	fitxer.close()
	
	print(comptador)

	
# Fes un programa que compti les paraules d'un fitxer passat per argument.
# Si no hi ha argument, s'ha d'utilitzar l'entrada estandard.	
if False:
	comptador = 0
	
	if len(sys.argv) == 2:
		fitxer = open(sys.argv[1],"r")
		
		for linia in fitxer:
			for char in linia:
				comptador +=1

	fitxer.close()
	
	print(comptador)

# Fes un programa que simuli la ordre cat.
if False:
	for arg in sys.argv[1:]:
		fitxer = open(arg,"r")
		for linia in fitxer:
			# .strip() per treure els salts de linia
			print(linia.strip())
	
		fitxer.close()


	
