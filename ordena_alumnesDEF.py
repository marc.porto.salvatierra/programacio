# !/usr/bin/python3
# -*-coding: utf-8-*-

#Programa per o ordenar segons diferents criteris una llista de llistes d'alumnes

#V1: Tenim les dades estructurades en una llista de llistes (guarrada)
#En la següent versió s'estructuraran amb diccionaris

F_ALUMNES = 'alumnes.txt'

def mostraAlumne(dni, d_alumne):
	'''
	Funció que ens mostra les dades d'un alumne d'aquesta manera:
	maria casas, dni: 72345678A,  mitjana: 5
	Entrada: llista d'alumne [dni, nom, cognom, [nota1, nota2, nota3]]
	Sortida: str
	'''
	nom_al = d_alumne['nom']
	cognom_al = d_alumne['cognom']
	mitjana_al = mitjana(d_alumne['notes'])
	return f'{nom_al} {cognom_al}, dni: {dni}, mitjana {mitjana_al}'
	
def mitjana(llista):
	'''
	Funció que calcula la mitjana d'una llista de floats
	Entrada: llista de floats
	Sortida: float
	'''
	suma = 0
	for n in llista:
		suma += n
	return suma / len(llista)

def llistat_Alumnes_dic(d_tots):
	'''
	Funció que em mostra tots els alumnes, un per línia, utilitzant la funció 
	mostraAlumne. Està ordenat segons l'ordre inical del diccionari
	Entrada: diccionari amb TOTS els alumnes
	sortida: res
	'''
	for alumne in d_tots:
		print(mostraAlumne(alumne, d_tots[alumne]))	
	print()	

def llistat_Alumnes_keys(keys, d_tots):
	'''
	Funció que em mostra tots els alumnes, un per línia, utilitzant la funció 
	mostraAlumne. Està ordenat segons l'ordre de la llista de keys
	Entrada: llista keys / diccionari d'alumnes
	sortida: res
	'''
	for k in keys:
		print(mostraAlumne(k, d_tots[k]))	
	print()
		
def llistat_Alumnes_taula(taula, d_tots):
	'''
	Funció que em mostra tots els alumnes, un per línia, utilitzant la funció 
	mostraAlumne. Està ordenat segons l'ordre de la taula
	Entrada: taula de tuples (k, camp) / diccionari d'alumnes
	sortida: res
	'''
	for el in taula:
		print(mostraAlumne(el[0], d_tots[el[0]]))
	print()	
		
def crea_taula(tots, camp):
	'''
	Funció que crea una taula (una llista de tuples) que relacionen la key del
	diccionari amb un camp
	'''		
	taula = []
	for k in tots:
		# busco el valor del 'camp' del diccionari corresponent a la clau k
		valor_camp = tots[k][camp]
		taula.append((k, valor_camp))
	return taula	

def take_second(elem):
	'''
	Key function per a ordenar segons el segon camp
	input: element a ordenar, enter que representa el numero del camp
	
	'''
	return elem[1]
    
def take_third(elem):
	'''
	Key function per a ordenar segons el tercer camp
	input: element a ordenar, enter que representa el numero del camp
	
	'''
	return elem[2]
	
def key_mitjana(elem):
	'''
	Key function per a ordenar segons la mitjana. Aquest element és una tupla
	amb una clau i una llista d'enters.
	input: element a ordenar, enter que representa el numero del camp
	
	'''
	return mitjana(elem[1])
	
def crea_diccionari_alumnes(f_f, dic_tots):
	'''
	input: dic_tots, fluxe fitxers(r)
	output: res
	'''

	for linia in f_f:
		linia = linia.strip()
		camps = linia.split(":")
	
		notes = []
		for n in camps[3].split():
			notes.append(int(n))
		
		dic_alumne = {'nom': camps[1], 'cognom': camps[2], 'notes': notes }
		dic_tots[camps[0]] = dic_alumne
	
def desa_dades_alumnes(f_f, dic_tots):
	'''
	input: fluxe de fitxers(w), dic_tots
	output: res
	'''
	for k in dic_tots:
		dic1 = dic_tots[k]
		nom = dic1['nom']
		cognom = dic1['cognom']
		notes = dic1['notes']
		notes1 = ""
		for i in notes:
			i = str(i)
			notes1 = notes1 + i + ' '
		f_f.write(f'{k}:{nom}:{cognom}:{notes1}\n')

def alta_alumne(dni, nom, cognom, notes, d_tots):
	'''
	input: dni_alumne, nom, cognom, notes, d_tots
	output: boolean
	'''
	
	if dni in d_tots:
		return False
		
	d_alumne = {'nom': nom, 'cognom': cognom, 'notes': notes}
	
	d_tots[dni] = d_alumne
	return True
def baixa_alumne(dni, d_tots):
	'''
	input: 
	output: boolean
	'''
	if dni not in d_tots:
		return False
		
	del d_tots[dni]
	return True

import sys
	

dadesAlumnes = [['87654321A', 'maria', 'pi', [1, 2, 3, 4]],
	['12345678A', 'jordi', 'perez', [1, 2, 3]], 
	['52345678A', 'manel', 'garcia', [10, 10, 9, 10]], ['62345678A', 'joana', 'vila', [10]],
	['72345678A', 'maria', 'casas', [5, 7]], 
	['11111111D', 'josep-lluís', 'márquez', [1, 7, 5, 4]]]

if __name__ == '__main__':
	if True:
	



		# crea diccionari	
		f = open(sys.argv[1],"r")
		d = {}
		crea_diccionari_alumnes(d, f)

		f.close()

		print(d)

		# desa dades
		f = open(sys.argv[1],"w")
		desa_dades_alumnes(f,d)

		f.close()








