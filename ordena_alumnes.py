import sys
import ordena_alumnesDEF
# Programa ordena segons cognom
STDIN = "-"



if len(sys.argv) == 1:
	nom_fitxer = STDIN
else:
	nom_fitxer = sys.argv[1]

f = sys.stdin	
if nom_fitxer != STDIN:
	f = open(sys.argv[1], 'r')


d_tots = {}
ordena_alumnesDEF.crea_diccionari_alumnes(f, d_tots)

if nom_fitxer != STDIN:
	f.close()

taula = ordena_alumnesDEF.crea_taula(d_tots,'cognom')

taula.sort(key = ordena_alumnesDEF.take_second)
print(taula)
ordena_alumnesDEF.llistat_Alumnes_taula(taula, d_tots)
